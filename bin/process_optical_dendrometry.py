from cavitools.frontend import DendroFrontend
from cavitools.file_manager import DendroFileManager
from cavitools.image_processor import DendroImageProcessor

def main():
    
    SAVE_INTERMEDIATES = False # saves raw image as a cropped and stabilized black and white version.
    STABILISATION = True # activates the image-stabilisation
    SMOOTHING_WINDOW =  50 # number of images the image-stabilisation uses to detect shifts. (https://github.com/OpenSourceOV/cavicapture.git).
    
    
    dendro_frontend = DendroFrontend()
    dendro_file_manager = DendroFileManager(dendro_frontend.set_input_dir())
    dendro_image_processor = DendroImageProcessor()

    dendro_file_manager.create_base_folder_structure(dendro_frontend.set_project_name(),SAVE_INTERMEDIATES)
    dendro_image_data_list = dendro_file_manager.set_image_data_list()

    cropping_dict = dendro_frontend.set_cropping(dendro_image_data_list[3].path, dendro_image_processor)
    threshold = dendro_frontend.set_dendro_threshold(dendro_image_data_list[3].path, cropping_dict,dendro_image_processor)

    dendro_image_data_list = dendro_image_processor.process_all_images(dendro_image_data_list, threshold, cropping_dict, SAVE_INTERMEDIATES, SMOOTHING_WINDOW, STABILISATION,filemanager = dendro_file_manager)

    dendro_file_manager.save_dendro_image_data_list_as_xlsx(dendro_image_data_list)
    dendro_file_manager.save_dendro_image_data_list_as_csv(dendro_image_data_list)

if __name__ == "__main__":
    main()