import tkinter.filedialog 
import cv2 

class Frontend:
    
    def set_input_dir(self):
        print("Choose input folder")
        
        input_dir = tkinter.filedialog.askdirectory()
        if input_dir == "":
            exit(0)
        else:
            return input_dir
        
    def set_project_name(self):
        project_name = input("Enter project name (if empty a name gets generated automatically):")
        if self.contains_invalid_characters(project_name):
            print("The text contains special characters. Please try other project name.")
            return self.set_project_name()
        else:
            return project_name
    
    def set_cropping(self, image_path, image_processor):
        cropping_dict = {"SmallPointY": None, "BigPointY": None, "SmallPointX": None, "BigPointX": None}
        image = cv2.imread(image_path)
        roi_manager = self.RoiManager(image)

        print("Select region of interest. To save selection press [space].")

        cv2.namedWindow('Roi', cv2.WINDOW_NORMAL)
        cv2.setMouseCallback('Roi',roi_manager.draw_rectangle)

        self.resize_window_with_aspect_ratio(image,"Roi",height=400)
        
        
        while True:
            
            cv2.imshow("Roi",roi_manager.canvas)
            k = cv2.waitKey(10) 

            if k == 27:
                break
                
            elif k == 32: #space
                cv2.destroyAllWindows()
                cv2.namedWindow('Roi', cv2.WINDOW_NORMAL)

                if all(v is not None for v in (roi_manager.start_point_x, roi_manager.start_point_y, roi_manager.end_point_x, roi_manager.end_point_y)):
                    SmallPointX = min(roi_manager.start_point_x, roi_manager.end_point_x)
                    BigPointX = max(roi_manager.start_point_x, roi_manager.end_point_x)
                    SmallPointY = min(roi_manager.start_point_y, roi_manager.end_point_y)
                    BigPointY = max(roi_manager.start_point_y, roi_manager.end_point_y)

                    cropping_dict["SmallPointY"] = SmallPointY
                    cropping_dict["BigPointY"] = BigPointY
                    cropping_dict["SmallPointX"] = SmallPointX
                    cropping_dict["BigPointX"] = BigPointX

                roi = image_processor.crop_image(cropping_dict, image)
                
                self.resize_window_with_aspect_ratio(roi,"Roi",height=400)
                
                cv2.imshow("Roi",roi)
                cv2.waitKey(0)
                
                break

            elif cv2.getWindowProperty('Roi',cv2.WND_PROP_VISIBLE) < 1:        
                break    
        

        cv2.destroyAllWindows()
        cv2.waitKey(1)
        return cropping_dict 

    

    
    class RoiManager:
        def __init__(self, image):
            self.image = image
            self.canvas = self.image.copy()
            self.start_point_x, self.start_point_y = None, None
            self.end_point_x, self.end_point_y = None, None
            self.moving_position_x, self.moving_position_y = None, None
            self.correcting_x, self.correcting_y = "", ""
            self.drawing = False
            self.moving = False
            
        def draw_rectangle(self, event, x, y, flags, param):

            if event == cv2.EVENT_LBUTTONDOWN:
                self.drawing = False
                self.moving = False
            
                self.correcting_x, self.correcting_y = "", ""
                self.moving_position_y, self.moving_position_y = None, None

                if self.end_point_x is not None and self.end_point_y is not None:

                    if (self.start_point_x < x < self.end_point_x or self.end_point_x < x < self.start_point_x) and \
                    (self.start_point_y < y < self.end_point_y or self.end_point_y < y < self.start_point_y):
                        self.moving = True
                        self.moving_position_x = x - self.start_point_x
                        self.moving_position_y = y - self.start_point_y

                    elif any(abs(coord - x) < 50 or abs(coord - y) < 50 for coord in [self.start_point_x, self.end_point_x, self.start_point_y, self.end_point_y]):
                        if abs(self.start_point_x - x) < 50:
                            self.correcting_x = "bx"
                        elif abs(self.end_point_x - x) < 50:
                            self.correcting_x = "ex"
                        if abs(self.start_point_y - y) < 50:
                            self.correcting_y = "by"
                        elif abs(self.end_point_y - y) < 50:
                            self.correcting_y = "ey"
        


                if not self.moving and not (self.correcting_x or self.correcting_y):

                    if all(v is not None for v in (self.start_point_x, self.start_point_y, self.end_point_x, self.end_point_y)):
                        self.start_point_x, self.start_point_y = None, None
                        self.end_point_x, self.end_point_y = None, None
                        
                    else:
                    
                        self.drawing = True
                        self.start_point_x, self.start_point_y = x, y
                        
            elif event == cv2.EVENT_MOUSEMOVE:
            
                if self.drawing:
                    self.end_point_x, self.end_point_y = x, y

                if self.moving:
                    self.end_point_x = (x - self.moving_position_x) + (self.end_point_x - self.start_point_x)
                    self.end_point_y = (y - self.moving_position_y) + (self.end_point_y - self.start_point_y)
                    self.start_point_x = x - self.moving_position_x
                    self.start_point_y = y - self.moving_position_y

                if self.correcting_x == "bx":
                    self.start_point_x = x
                elif self.correcting_x == "ex":
                    self.end_point_x = x

                if self.correcting_y == "by":
                    self.start_point_y = y
                elif self.correcting_y == "ey":
                    self.end_point_y = y
                
                self.canvas = self.image.copy()

                if self.end_point_x is not None or self.end_point_y is not None:
                    
                    cv2.rectangle(self.canvas, (self.start_point_x, self.start_point_y), (self.end_point_x, self.end_point_y), (148, 255, 0), 10)

            elif event == cv2.EVENT_LBUTTONUP:
                self.drawing = False
                self.moving = False
                self.correcting_x, self.correcting_y = "", ""
                self.moving_position_x, self.moving_position_y = None, None
                if self.end_point_x is not None or self.end_point_y is not None:
                    if abs(self.start_point_x - self.end_point_x) > 10 and abs(self.start_point_y - self.end_point_y) > 10:

                        cv2.rectangle(self.canvas, (self.start_point_x, self.start_point_y), (self.end_point_x, self.end_point_y), (148, 255, 0), 10)

    def resize_window_with_aspect_ratio(self,image,window, width=None, height=None, inter=cv2.INTER_AREA):
        (h, w) = image.shape[:2]

        if width is None and height is None:
            return image
        
        if width is None:
            r = height / float(h)
            return cv2.resizeWindow(window, (int(w * r),height))
        
        else:
            r = width / float(w)
            return cv2.resizeWindow(window, width, int(h * r))
        
    def contains_invalid_characters(self,text):
        invalid_characters = ['\\', '/', ':', '*', '?', '"', '<', '>', '|']
        for char in text:
            if char in invalid_characters:
                return True
        return False
    
    @staticmethod
    def progress_bar(current, total, bar_length=20):
        fraction = current / total

        arrow = int(fraction * bar_length - 1) * '-' + '>'
        padding = int(bar_length - len(arrow)) * ' '

        ending = '\n' if current == total else '\r'

        print(f'Progress: [{arrow}{padding}] {int(fraction*100)}%', end=ending)

class CaviFrontend(Frontend):
    
    def set_cavitation_image_path(self):
        print("Choose an image with a clear cavitation event from your selection to determine suitable processing parameters")

        cavitation_image_path = tkinter.filedialog.askopenfilename()
        if cavitation_image_path == "":
            exit(0)
        else:   
            return cavitation_image_path

    def set_mode(self):
        try:
            input_transmissive_mode = input("Transmissive or reflective mode? (t/r) ")

            if input_transmissive_mode == "T" or input_transmissive_mode == "t":
                return True
            
            elif(input_transmissive_mode == "R" or  input_transmissive_mode ==  "r" ):
                return False
        except:
            print("Error. Please try again")
            return self.set_mode()

    def set_processing_parameters(self, cavi_image_data_list, cropping_dict, transmissive_mode, cavi_image_processor, filemanager):
    
        input_manual_parameters = input("Find processing parameters by generating previews? (y/n): ")
        
        if input_manual_parameters.lower() == "y":
        
            return self.set_processing_parameters_with_previews(cavi_image_data_list, cropping_dict, transmissive_mode, cavi_image_processor, filemanager)
        
        elif input_manual_parameters.lower() == "n":
            try:
                
                threshold = int(input("Please enter your preferred threshold value: "))
                noise_reduction = int(input("Please enter your preferred noise reduction value (odd): "))
                
            
                if noise_reduction % 2 == 1:
                    return threshold, noise_reduction
                else:
                    print("Noise reduction value must be an odd number.")
                    
            except ValueError:
                
                print("Invalid input. Please enter integer values.")

        else:
            print("Invalid option. Please enter 'y' or 'n'.")

        return self.set_processing_parameters(cavi_image_data_list, cropping_dict, transmissive_mode, cavi_image_processor, filemanager)

    def set_processing_parameters_with_previews(self,cavi_image_data_list,cropping_dict,mode,cavi_image_processor,filemanager):
        index = self.get_cavitation_image_index(cavi_image_data_list)
        print("Processing...")
        previews = cavi_image_processor.create_previews(cropping_dict, mode, index, cavi_image_data_list,filemanager)
        print("Finished")
        print("With the keys 'a' and 'd', you can switch between the previews with certain processing parameters. Decide on one parameter set and proceed with [space]") 
        pointer = 0
        show_image = previews[pointer][1]

        cv2.namedWindow('T&N', cv2.WINDOW_NORMAL)
        self.resize_window_with_aspect_ratio(show_image,"T&N",height=900)

        while True:

            cv2.imshow("T&N",show_image)
            cv2.putText(show_image,previews[pointer][0],(20,80),cv2.FONT_HERSHEY_DUPLEX,2,255,1,cv2.LINE_AA)

            k = cv2.waitKey(10) 
            if k == 27:
                break

            elif cv2.getWindowProperty('T&N',cv2.WND_PROP_VISIBLE) < 1:
                break

            elif k == ord("d"):
                if pointer < len(previews)-1:
                    pointer += 1
                    show_image = previews[pointer][1]

            elif k == ord("a"): 
                if pointer > 0:
                    pointer -= 1
                    show_image = previews[pointer][1]
                
            elif k == 32: #space
                cv2.destroyAllWindows()
                cv2.waitKey(1)
                break
        
        if input("Try another image? (y/n)").lower() == "y":
            return self.set_processing_parameters_with_previews(cavi_image_data_list,cropping_dict,mode,cavi_image_processor,filemanager)
        
        else:
            return previews[pointer][2], previews[pointer][3]
        
        

    def get_cavitation_image_index(self,cavi_image_data_list):

        image_path = self.set_cavitation_image_path()
        for index, img_data in enumerate(cavi_image_data_list):
            if img_data.path == image_path:
                return index
        
        print("Couldn't find image in input folder")

        return self.get_cavitation_image_index()
    
class DendroFrontend(Frontend):

    def set_dendro_threshold(self,image_path,cropping_dict,image_processor):
        
        print("With the keys 'a' and 'd', you can change the threshold applied to the image. Decide on one threshold and proceed with [space]")

        threshold = 100
        
        image = cv2.imread(image_path,0)
        image = image_processor.crop_image(cropping_dict,image)
        cv2.namedWindow('Threshold', cv2.WINDOW_NORMAL)
        self.resize_window_with_aspect_ratio(image,"Threshold",height=600)
        
        
        while True:
            show_image = image.copy()
            show_image[show_image<threshold] = 0
            text = "T:" + str(threshold)
            cv2.putText(show_image,text,(20,80),cv2.FONT_HERSHEY_DUPLEX,2,150,2,cv2.LINE_AA)
            
            cv2.imshow("Threshold",show_image)

            k = cv2.waitKey(10)

            if k == 27:
                exit(0)

            elif cv2.getWindowProperty('Threshold',cv2.WND_PROP_VISIBLE) < 1:
                exit(0)

            elif k == ord("d"):
                if threshold<255:
                    threshold += 1
                

            elif k == ord("a"):
                if threshold>0:
                    threshold -= 1

            elif k == 32: #space    
                cv2.destroyAllWindows()

                processed_image = image_processor.process_image(image,threshold)

                cv2.namedWindow('Threshold', cv2.WINDOW_NORMAL)
                self.resize_window_with_aspect_ratio(processed_image,"Threshold",height=600)
                cv2.imshow("Threshold",processed_image)
                cv2.waitKey(0)
                
                break
                
        cv2.destroyAllWindows()
        cv2.waitKey(1)
        return threshold
        

    


    

    

