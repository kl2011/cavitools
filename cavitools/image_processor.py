import cv2
import numpy as np 
from cavitools.frontend import Frontend
from vidstab.VidStab import VidStab
from time import time
from datetime import datetime

class ImageProcessor():

    def setup_stabilizer(self,stabilizer,img_list,smoothing_window = 10):
        if smoothing_window > len(img_list)-1:
            smoothing_window = len(img_list)-1
        
        for index in range(0,smoothing_window):

            processed_image = cv2.imread(img_list[index].path, 0)
            stabilizer.stabilize_frame(processed_image,smoothing_window)

        return smoothing_window
    def stabilize_img(self,stabilizer,img_list,index,smoothing_window):

        if(len(img_list) > index +  smoothing_window):
            image = cv2.imread(img_list[index + smoothing_window].path, 0)
        else:
            image = cv2.imread(img_list[-1].path, 0)
        stabilized_img = stabilizer.stabilize_frame(image,smoothing_window)

        return stabilized_img

    def crop_image(self, cropping_dict, image):
        no_none_value = all(value is not None for value in cropping_dict.values())

        if no_none_value:
            image = image[cropping_dict["SmallPointY"]:cropping_dict["BigPointY"],cropping_dict["SmallPointX"]:cropping_dict["BigPointX"]]
        else:
            image = image[30:-30,30:-30]
    
        return image
    
    def compute_time_intervals(self,image_name,compare_datetime):

        try:    
                numeric_part = ''.join(c for c in image_name if c.isdigit())
                current_datetime = datetime.strptime(numeric_part,"%Y%m%d%H%M%S")

                if compare_datetime is not None:
                    time_interval = int((round((current_datetime-compare_datetime).total_seconds())))
                    
                else:
                    time_interval = 0

        except:
            current_datetime = None
            time_interval = 0
            
        return time_interval, current_datetime

class CaviImageProcessor(ImageProcessor):

    def processAllImages(self, cavi_image_data_list,labeling, threshold, noise_reduction, cropping_dict, transmissive_mode, save_intermediates, smoothing_window,filemanager):
        t0 = time() 
        print("Process all images...")

        stabilizer = VidStab()
        smoothing_window = self.setup_stabilizer(stabilizer,cavi_image_data_list,smoothing_window)
    
        for index, cavi_image_data in enumerate(cavi_image_data_list):

            Frontend.progress_bar(index,len(cavi_image_data_list))
            image = self.stabilize_img(stabilizer, cavi_image_data_list, index, smoothing_window)
            image = self.crop_image(cropping_dict, image)

            if index == 0:
                previous_image = image
                compare_datetime = None
                
            else:
                processed_image = self.process_image(image, previous_image, transmissive_mode, threshold, noise_reduction)
                cavi_image_data,compare_datetime = self.set_image_data_attributes(processed_image,cavi_image_data,compare_datetime,labeling)

                if save_intermediates:
                    filemanager.save_intermediate_image(cavi_image_data.name, image)
                
               
                filemanager.save_cavi_image(cavi_image_data,labeling,processed_image,previous_image,image)
               

                previous_image = image

        print()
        t1 = time()
        print("Finished in " + str(int(t1-t0)) + " Seconds")

        return cavi_image_data_list
    
    def label_processed_stack(self,image_data_list,frontend,filemanager):
        compare_datetime = None

        for index, image_data in enumerate(image_data_list):
        
            frontend.progress_bar(index,len(image_data_list))

            processed_image = cv2.imread(image_data.path, 0)

            image_data,compare_datetime = self.set_image_data_attributes(processed_image,image_data,compare_datetime,True)
            
            filemanager.save_cavi_image_based_on_label(image_data,processed_image)

        return image_data_list
    
    def process_image(self, image,previous_image,transmissive_mode,threshold,noisereduction):
        if transmissive_mode:
            processed_image = cv2.subtract(previous_image, image)
            
        else:
            processed_image = cv2.subtract(image, previous_image)

        processed_image = self.apply_threshold(processed_image,threshold) 
        processed_image = self.median_filter_using_median_blur(processed_image,noisereduction)
        processed_image = self.apply_bw(processed_image)

        return processed_image
        

    def label_image(self, processed_image):
        label = "no event"

        image_marked_events = processed_image.copy()
        height, width = processed_image.shape
        image_size = height * width
        image_ratio = image_size / 1500000
        processed_image_area = cv2.countNonZero(processed_image)
        area_without_contours = processed_image_area

        #close vertical gaps between pixel
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3,60))
        image_closed_gaps = cv2.morphologyEx(processed_image, cv2.MORPH_CLOSE, kernel)
        num_connected_components,labels = cv2.connectedComponents(processed_image)

        if num_connected_components != 0:
            contours, hierarchy= cv2.findContours(image_closed_gaps, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        
            for contour in contours :
                if cv2.contourArea(contour) > 600 * image_ratio: #ignore dilated contours under 1000 for faster processing (double of area_isolated_contour_threshold)
                
                    width, height, angle,aspect_ratio,area_isolated_contour,num_gaps,box = self.get_contour_infos(contour,processed_image)
                    
                    if(area_isolated_contour > 300 * image_ratio): #ignore small events 
                        if (aspect_ratio > 4) and  ((width > height and angle > 80) or (height>width and angle < 20)): #checks aspect ratio and alignment of contour 
                            
                            if(aspect_ratio > 7) and (area_isolated_contour/num_gaps < 100) and (label != "uncertain"): #for clear events: checks aspect ratio, pixel density and for other events on the image
                                cv2.drawContours(image_marked_events,[box],0,(255),2)
                               
                                area_without_contours -= area_isolated_contour
                                label = "cavitation"
                                
                            elif((area_isolated_contour/num_gaps < 300)): 
                                cv2.drawContours(image_marked_events,[box],0,(100),2) 
                                label  = "uncertain"

                        elif label == "cavitation":
                            label  = "uncertain"

        if label == "cavitation":
          
            if (area_without_contours/processed_image_area > 0.20 or num_connected_components > 500 * image_ratio): #checks if pixels that are not part of events are less than 20 percent of image and for to many pixel segments
                label = "uncertain"
                
            else:
                return label,image_marked_events
            
        if label == "uncertain":

            return label, image_marked_events
        else:
            return label , image_marked_events
        
    def set_image_data_attributes(self,processed_image,cavi_image_data,compare_datetime,labeling):
        if(labeling):
            label, image_marked_events = self.label_image(processed_image)
            cavi_image_data.set_label_attributes(label)

        cavi_image_data.set_area_attributes(cv2.countNonZero(processed_image))
        time_interval, compare_datetime = self.compute_time_intervals(cavi_image_data.name, compare_datetime)
        cavi_image_data.set_time_attributes(time_interval)
    
        return cavi_image_data,compare_datetime
    
    def get_contour_infos(self,contour, processed_image):

        #get contour shape
        rect = cv2.minAreaRect(contour)
        (x, y), (width, height), angle = rect
        aspect_ratio = max(width, height) / min(width, height)
        box = cv2.boxPoints(rect) 
        box = np.intp(box)

        #isolate contour
        without_contour = processed_image.copy()
        cv2.drawContours(without_contour,[contour],0,(0, 0, 0), thickness=cv2.FILLED)
        isolated_contour = processed_image.copy() - without_contour
        area_isolated_contour = cv2.countNonZero(isolated_contour)

        #count gaps in contour
        inverted_isolated_contour = cv2.bitwise_not(isolated_contour)
        num_gaps, labels = cv2.connectedComponents(inverted_isolated_contour,connectivity = 4)
        
        return width, height, angle,aspect_ratio,area_isolated_contour,num_gaps,box

    def apply_threshold(self,image,threshold):
        image[image<threshold] = 0
        return image

    def median_filter_using_median_blur(self, image,noise_reduction):
        median_blurred_image = cv2.medianBlur(image,noise_reduction)
        return cv2.min(image, median_blurred_image)

    def apply_bw(self, image):
        ret2,bw_image = cv2.threshold(image,0,255,cv2.THRESH_BINARY)
        return bw_image
    
    def create_previews(self, cropping_dict ,mode ,index, cavi_image_data_list, filemanager):
        previews = []
        image = cv2.imread(cavi_image_data_list[index].path,0)
        previous_image = cv2.imread(cavi_image_data_list[index-1].path,0)

        image = self.crop_image(cropping_dict, image)
        previous_image = self.crop_image(cropping_dict, previous_image)

        #change range of for-loops for other preview values
        for threshold in range(2,7):
            for noise_reduction in range(3,11):
                if(noise_reduction % 2 == 1):

                    preview_name = "T" + str(threshold) + "N" + str(noise_reduction)
                    processed_image = self.process_image(image,previous_image,mode,threshold,noise_reduction)
                    previews.append((preview_name, processed_image,threshold, noise_reduction))
                    
        sorted_previews = sorted(previews, key=lambda x: cv2.connectedComponents(x[1])[0])
        filemanager.save_previews(sorted_previews)

        return sorted_previews

class DendroImageProcessor(ImageProcessor):

    def process_all_images(self, dendro_image_data_list,threshold,cropping_dict, save_intermediates, smoothing_window, stabilisation, filemanager):
        t0 = time()
        print("Process all images...")
        

        stabilizer = VidStab()
        smoothing_window = self.setup_stabilizer(stabilizer,dendro_image_data_list,smoothing_window)

        for index, dendro_image_data in enumerate(dendro_image_data_list):

            Frontend.progress_bar(index,len(dendro_image_data_list))
            if index == 0:
                compare_datetime = None

            if stabilisation:
                image = self.stabilize_img(stabilizer,dendro_image_data_list,index,smoothing_window)
            else:
                image = cv2.imread(dendro_image_data.path, 0)
                
            image = self.crop_image(cropping_dict, image)
            processed_image = self.process_image(image,threshold)
            image_height,_ = processed_image.shape
            dendro_image_data,compare_datetime = self.set_image_data_attributes(processed_image, image_height, dendro_image_data, compare_datetime)
    
            if save_intermediates:
                filemanager.save_intermediate_image(dendro_image_data.name, image)
                
            filemanager.save_dendro_image(dendro_image_data, processed_image)

        print()
        t1 = time()
        print("Finished in " + str(int(t1-t0)) + " Seconds")
        return dendro_image_data_list

    def process_image(self, image,threshold):
        image[image<threshold] = 0
        ret2,bw_image = cv2.threshold(image,0,255,cv2.THRESH_BINARY)
        return bw_image

    def set_image_data_attributes(self,processed_image, image_height,dendro_image_data,compare_datetime):

        dendro_image_data.area = cv2.countNonZero(cv2.bitwise_not(processed_image))
        dendro_image_data.width_mean = dendro_image_data.area / image_height
        time_interval, compare_datetime = self.compute_time_intervals(dendro_image_data.name, compare_datetime)
        dendro_image_data.set_time_attributes(time_interval)

        return dendro_image_data,compare_datetime

