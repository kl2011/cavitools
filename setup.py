from setuptools import setup,find_packages

setup(
    name='cavitools',
    version='1.0',
    packages= find_packages(),
    install_requires=[
        'numpy',
        'opencv-python>=4.9.0.80',
        'XlsxWriter>=3.1',
        'vidstab>=1.7.4'
      ],
      entry_points={
        "console_scripts":["process_stem_cavitation=bin.process_stem_cavitation:main", "process_leaf_cavitation=bin.process_leaf_cavitation:main", "process_optical_dendrometry=bin.process_optical_dendrometry:main"]

    },
)

