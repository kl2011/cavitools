class ImageData:
    uncertain_counter = 0
    total_time = 0

    def __init__(self,path,name):
        self.path = path
        self.name = name
        self.excel_label = "None"
        self.label = "None"
        
    def set_time_attributes(self, time_interval):
        self.time_interval = time_interval
        ImageData.total_time += time_interval
        self.time = ImageData.total_time

        
class CaviImageData(ImageData):
    total_area = 0
    
    def set_area_attributes(self, area):
        self.area = area
        CaviImageData.total_area += area
        self.cum_area = CaviImageData.total_area

    def set_label_attributes(self, label):
        self.label = label
        if label == "uncertain":
            self.excel_label = label + "_img" + str(ImageData.uncertain_counter)
            ImageData.uncertain_counter += 1
        else:
            self.excel_label = label

class DendroImageData(ImageData):

    def set_label_attributes(self,label):
        pass