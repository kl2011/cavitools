from cavitools.image_data import CaviImageData, DendroImageData
from pathlib import Path
from shutil import rmtree
import xlsxwriter.workbook
import csv
import cv2
import re

class FileManager:

    def __init__(self,input_dir):
        self.input_dir = Path(input_dir)

    def set_image_data_list(self):
        image_data_list = []
    
        paths = [p for p in self.input_dir.iterdir() if p.suffix in [".png", ".tif", ".tiff"]]

        if paths:
            sorted_paths = sorted(paths, key=lambda path: [int(integer) for integer in re.findall(r'\d+', path.stem)])

            for path in sorted_paths:
                image_data = self.create_image_data_instance(path)
                image_data_list.append(image_data)

            return image_data_list
        
        else:
            print("No images in .png, .tif, or .tiff format were found")
            exit(0)
        
    def create_base_folder_structure(self, project_name, save_intermediates):
        parent_folder = self.input_dir.parent

        if project_name == "":
            self.project_name = self.input_dir.name
            self.project_name= str("Results_" + self.project_name)
        else:
            self.project_name = project_name
    
        self.project_folder = parent_folder / self.project_name 
        
        
        processed_dir = self.project_folder / "Processed"
        self.createEmptyDir(processed_dir)
        self.processed_dir = str(Path(processed_dir))

        if save_intermediates:
            intermediates_dir = self.project_folder /"Intermediates"
            self.createEmptyDir(intermediates_dir)
            self.intermediates_dir= str(Path(intermediates_dir))


    def createEmptyDir(self, dir):

        try:
            dir.mkdir(parents=True, exist_ok=False)
        except:

            try:
                rmtree(dir)
                dir.mkdir(parents=True, exist_ok=False)
            except: 
                print("Error")
                exit(0)
                
    def save_intermediate_image(self, name, image):
        file_name = "/Intermediate-" + name
        cv2.imwrite(self.intermediates_dir + file_name, image)
        

    def create_csv_writer(self):
        csv_file_path = self.project_folder / (self.project_name + ".csv")

        try: 
            csv_file = open(csv_file_path, 'w',newline='')

        except:
            csv_file = open(self.file_writer_exception_handling(csv_file_path,1), "w",newline='')

        return csv.writer(csv_file)
    
    def create_xlsx_writer(self): 
        xlsx_file_path = self.project_folder / (self.project_name + ".xlsx")

        try:
            if xlsx_file_path.is_file():
                xlsx_file_path.unlink()

        except:
            xlsx_file_path = self.file_writer_exception_handling(xlsx_file_path,1)

        return xlsxwriter.Workbook(xlsx_file_path)

    def file_writer_exception_handling(self,path,c):
        suffix = path.suffix
        file_name = self.project_name +  "[" + str(c) + "]" + suffix
        path = self.project_folder /  file_name

        if path.is_file():
            c += 1
            return self.file_writer_exception_handling(path,c)
        else:
            
            return path
        
    def create_image_data_instance(self, path):
        raise NotImplementedError("Subclasses must implement create_image_data_instance method.")
    
class CaviFileManager(FileManager):

    
    def create_folder_structure(self,project_name, save_intermediates, labeling):
        super().create_base_folder_structure(project_name,save_intermediates)

        preview_dir = self.project_folder / "Preview"
        preview_dir.mkdir(parents=True, exist_ok=True)
        self.preview_dir = str(Path(preview_dir))

        if labeling:
            cavitation_events_dir = self.project_folder / "Cavitation Events"
            self.createEmptyDir(cavitation_events_dir)
            self.cavitations_events_dir = str(Path(cavitation_events_dir))

            uncertain_dir = self.project_folder / "Uncertain" 
            self.createEmptyDir(uncertain_dir)
            self.uncertain_dir = str(Path(uncertain_dir))

            no_events_dir = self.project_folder / "No Events"
            self.createEmptyDir(no_events_dir)
            self.no_events_dir = str(Path(no_events_dir))

    def save_cavi_image(self, cavi_image_data, labeling, processed_image, previous_image=None, image=None):
        file_name = "/Processed-" + cavi_image_data.name
        cv2.imwrite(self.processed_dir + file_name,processed_image)
        
        if labeling:
            if cavi_image_data.label == "cavitation":
                cv2.imwrite(self.cavitations_events_dir + file_name,processed_image)


            elif cavi_image_data.label == "uncertain":
                cv2.imwrite(self.uncertain_dir + "/" + cavi_image_data.excel_label + "_P.png",processed_image)
                
                if previous_image is not None and image is not None:
                    cv2.imwrite(self.uncertain_dir + "/" + cavi_image_data.excel_label + "_R1.png",previous_image)
                    cv2.imwrite(self.uncertain_dir + "/" + cavi_image_data.excel_label + "_R2.png",image)
                
            elif cavi_image_data.label == "no event":
                cv2.imwrite(self.no_events_dir + file_name,processed_image)

            
    def save_cavi_image_data_list_as_xlsx(self,cavi_image_data_list):
        xlsx_file = self.create_xlsx_writer()
        cavi_image_data_list.pop(0)
        cavi_classified_image_data = [image for image in cavi_image_data_list if (image.excel_label != 'no event' and image.excel_label != "None")]
        
        if len(cavi_classified_image_data) > 0:
            classified_worksheet = xlsx_file.add_worksheet("Classified")
            self.fill_worksheet(xlsx_file, classified_worksheet, cavi_classified_image_data)
        
        raw_worksheet = xlsx_file.add_worksheet("Raw")
        self.fill_worksheet(xlsx_file, raw_worksheet, cavi_image_data_list)

        xlsx_file.close()
    
    def fill_worksheet(self, xlsx_file, worksheet, image_data_list):
        
        cell_font_green = xlsx_file.add_format()
        cell_font_green.set_font_color('green') 
        cell_font_red = xlsx_file.add_format()
        cell_font_red.set_font_color('red')  
        cell_bold = xlsx_file.add_format()
        cell_bold.set_bold()
        worksheet.set_column(1,1,15)
        worksheet.set_column(2,3,10)
        worksheet.set_column(6,7,13)
        worksheet.set_column(8,8,9)
        worksheet.set_column(9,9,12)
        worksheet.write_row(0,0,("Number","Name","TimeInterval","Time[sec]","Area","CumArea","RelArea[%]","Label"),cell_bold)

        for index, cavi_image_data in enumerate (image_data_list,start=1):
            worksheet.write_number(index,0,index) #Name
            worksheet.write(index,1,cavi_image_data.name) #Timestamp

            if(cavi_image_data.time_interval is None):
                worksheet.write_number(index,2,0) #Interval
                worksheet.write_number(index,3,0) #Time
            else:
                worksheet.write_number(index,2,cavi_image_data.time_interval) #Interval
                worksheet.write_number(index,3,cavi_image_data.time,cell_font_green)#Time

            worksheet.write_number(index,4,cavi_image_data.area,cell_font_green) #Area
        
            if index == 1:
                worksheet.write_formula(index,5,"E2") #CumArea
            else:
                worksheet.write_formula(index,5,"=E"+ str(index+1) +"+ F" + str(index)) #CumArea

            worksheet.write_formula(index,6,"=F"+ str(index+1) + "/" + "F"+ str(len(image_data_list)),cell_font_green) #RelArea
            worksheet.write(index,7,cavi_image_data.excel_label)
            
        worksheet.autofilter(0,7,len(image_data_list),7)
        

    def save_cavi_image_data_list_as_csv(self, cavi_image_data_list):
        writer = self.create_csv_writer()

        header = ["Index", "Name", "Interval", "Time", "Area", "Cumulative Area", "RelArea", "Label"]
        writer.writerow(header)

        for index, cavi_image_data in enumerate(cavi_image_data_list):
         
            row_data = [
                index,
                cavi_image_data.name,
                cavi_image_data.time_interval if cavi_image_data.time_interval is not None else 0,
                cavi_image_data.time,
                cavi_image_data.area,
                cavi_image_data.cum_area,
                round(cavi_image_data.cum_area / cavi_image_data.total_area * 100, 2),
                cavi_image_data.excel_label
            ]
            writer.writerow(row_data)

    def create_image_data_instance(self, path):
        return CaviImageData(str(path).replace("\\", "/"), str(path.name))

    def save_previews(self, previews):
        for index,preview in enumerate(previews):  
  
            file_name = "/Preview_"+ str(index)+ "_" + preview[0] + ".png"
            cv2.imwrite(self.preview_dir + file_name, preview[1])
        

class DendroFileManager(FileManager):
    
    def save_dendro_image(self,dendro_image_data,processed_image):
        file_name = "/Processed-" + dendro_image_data.name
        cv2.imwrite(self.processed_dir + file_name,processed_image)

    
    def save_dendro_image_data_list_as_xlsx(self,dendro_image_data_list):
        xlsx_file = self.create_xlsx_writer()
        worksheet = xlsx_file.add_worksheet("Processed")

        cell_font_green = xlsx_file.add_format()
        cell_font_green.set_font_color('green') 
        cell_font_red = xlsx_file.add_format()
        cell_font_red.set_font_color('red')  
        cell_bold = xlsx_file.add_format()
        cell_bold.set_bold()
        worksheet.set_column(1,1,15)
        worksheet.set_column(2,3,10)
        worksheet.write_row(0,0,("Number","Name","Interval","Time[sec]","Width_Mean(in px)"),cell_bold)

        for index, dendro_image_data in enumerate (dendro_image_data_list[1:], start=1):
            worksheet.write_number(index,0,index) #Number
            worksheet.write(index,1,dendro_image_data.name) #Name

            if(dendro_image_data.time_interval is None):
                worksheet.write_number(index,2,0) #Interval
                worksheet.write_number(index,3,0) #Time
            else:
                worksheet.write_number(index,2,dendro_image_data.time_interval) #Interval
                worksheet.write_number(index,3,dendro_image_data.time,cell_font_green)#Time

            worksheet.write_number(index,4,dendro_image_data.width_mean,cell_font_green) #Area
            
        xlsx_file.close()

    def save_dendro_image_data_list_as_csv(self, dendro_image_data_list):
        writer = self.create_csv_writer()
        header = ["Index", "Name", "Interval", "Time", "Width_Mean(in px)"]
        writer.writerow(header)

        for index, dendro_image_data in enumerate(dendro_image_data_list[1:], start=1):
         
            row_data = [
                index,
                dendro_image_data.name,
                dendro_image_data.time_interval if dendro_image_data.time_interval is not None else 0,
                dendro_image_data.time,
                dendro_image_data.width_mean,
            ]
            writer.writerow(row_data)

    def create_image_data_instance(self, path):
        return DendroImageData(str(path).replace("\\", "/"), str(path.name))