# CaviTools
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.13328771.svg)](https://doi.org/10.5281/zenodo.13328771)

CaviTools offers automated processing scripts (Python, terminal-based) to detect and quantify embolism events in image time series of plant xylem material (stems or leaves) based on the Optical Method and to analyse optical dendrometry image sequences. 
Our work adds to the efforts of the [OpenSourceOV](https://www.opensourceov.org/) community and aims to facilitate image analysis, in particular for large/challenging data sets.
The tools build on Chris Lucani's work at [OpenSourceOV/cavicapture](https://github.com/OpenSourceOV/cavicapture.git) and use the image stabilisation software [vidstab](https://github.com/AdamSpannbauer/python_video_stab) by Adam Spannbauer.


## Features:

To process and analyze image stacks based on the optical method can be challenging, especially with large datasets. Additionally, we have found that not all brightness differences between images correspond to cavitation events. Camera shifts, resin flow, and insects passing through the frame can lead to wrong events. Manually reviewing all images to filter out these artefacts can be very time-consuming. Therefore, we offer the following tools to simplify the workflow of post-processing your data.

- Automated analysis determining the percentage of cavitated xylem area in stem- or leaf image time series
    - Possibility to individually calibrate processing parameters handling noise
    - Classifier that detects <ins>vertical</ins> stem embolism events and automatically cleans results from artefacts
- Automated analysis of area change in optical dendrometry image time series    
- Output: Processed images and result datasheets (.xlsx, .csv) 
- Resource efficiency: around 250 images per minute are processed utilizing approximately 300MB of RAM  

## Installation:

Before installing the package, please ensure Python 3 is installed on your device. More information about Python and how to install it can be found here: [python.org](https://www.python.org/)

Open a terminal window and enter the following command to install the package with all its dependencies:

```bash
pip install git+https://codebase.helmholtz.cloud/kl2011/cavitools
```

The three main functionalities are:

- `process_stem_cavitation`: processing and detecting cavitation events on stem xylem images
- `process_leaf_cavitation`: processing of stem or leaf xylem images (transmissive or reflective) [Does not detect cavitation events (yet)]
- `process_optical_dendrometry`: processing of optical dendrometry images

You automatically download these scripts when installing the package. Open a terminal and enter the `scriptname` to execute it.

For easier distribution in your facility, you can build your executables of this package, so even people without Python installed can run these scripts. More information here: [pyinstaller.org](https://pyinstaller.org/en/stable/).

On MacOs you may have to install tkinter manually, especially if you installed python via homebrew and use pyenv.

## Instructions:
All the scripts are terminal-based. However, for some tasks like setting the region of interest (ROI) or defining threshold values interactive windows will open.

The basic workflow with all scripts is the same. After selecting the directory storing your raw images and setting other parameters, an output folder is generated next to your selected input folder. It includes the processed images and an Excel as well as a CSV file with the final results.

For more information on generated sheets, please go to [Xylem Analysis](#xylem-analysis) or [Dendro Analysis](#dendro-analysis)


### Basic use of the xylem processing scripts:
This guide applies to "process_stem_cavitation" and "process_leaf_cavitation".

For better understanding, texts written in `courier` are the original instructions you see on the terminal when executing the program. To confirm an answer and continue press enter.

- Start execution by entering the script name in a terminal window.

- `Choose input folder`: Select the folder including the raw images you wish to process in the appearing window. Please make sure all images are in the same format (.png, .tif or .tiff).

- `Transmissive or reflective mode?`: (only process_label_leaf) Press [t] for transmissive images, [r] for reflective images

- `Enter project name`: Specify the name of the automatically generated output folder. If left empty, the folder will be named "Results_[Inputfolder]".

- `Select region of interest`: A window will appear showing one image of your selected image stack. You can drag the mouse to mark a ROI. To save our selection press [space].

- `Find processing parameters by generating previews?`:
    - By pressing [y], you need to provide an image of your choice displaying a clear embolism event and select it in the appearing window. You'll then see a window displaying one processed version of the selected image. Use [a] and [d] to toggle between different versions, each created with a unique combination of threshold and noise reduction values. Press space to select the processing parameters of the displayed image and initiate the processing of all images. In case you do not know which of your raw images includes an embolism event or if there are any at all, you can start a first run with default parameters (threshold value of 5 and a noise reduction value of 9), identify an embolism event in the output and start a second run in which you optimize the processing parameters with the preview function.
    - By pressing [n], you can directly input processing parameters. In most cases, a threshold value of 5 and a noise reduction value of 9 provide sufficiently good results. Then, the processing of all images will start. 

### Basic use of the dendro processing script:
This guide applies to "process_optical_dendrometry".

For better understanding texts written in `courier` are the original instructions you get in terminal when executing the program. To confirm an answer press [enter].

- Start execution by entering "process_optical_dendrometry" in a terminal window.
- `Choose input folder`: Select the folder including the raw images you wish to process in the appearing window. Please make sure all images are in the same format (.png, .tif or.tiff).

- `Enter project name`: Specify the name of the automatically generated output folder. If left empty, the folder will be named "Results_[Inputfolder]".

- `Select region of interest`: A window will appear showing one image of your selected image stack. You can drag the mouse to mark a ROI. To save our selection press [space]

- `Set threshold`: A window will display one image from your selected stack. Use [a] and [d] to adjust the applied image threshold. A sensible threshold balances the effort to mark pixels within the stem area but not next to it, particularly focusing on the edges. Press [space] to initiate the processing of all images with the selected threshold value.

## Xylem Analysis:
After the script has finished, results are saved in the following structure in the generated output directory:
- The "Processed" folder contains the processed images 
- The "Previews" folder contains all generated previews
- Results are written in a .csv and a .xlsx file and include the following parameters for each processed image: index, image name, time interval between captures, total time, embolism area, cumulated area, and relative cumulated area in percent


### Workflow with Classifier Results:
Image stacks processed with process_label_stem are labelled automatically by the inbuild classifier. It assigns one of three possible labels for each processed image: 

- "cavitation event" (indicating a cavitation event was detected)
- "no event" (indicating no event occurred)
- "uncertain" (indicating that brightness differences in the image could not be reliably classified and require an individual check).

For each label, an individual folder is created. Additionally, both the .csv and .xlsx files contain an extra column to mark the label of each image. Also, there is a second worksheet named "Classified" in the Excel file that just stores the values of images labelled with "cavitation event" or "uncertain".

Images labelled "uncertain" should be checked manually to decide if they contain an embolism event or brightness differences of other origins, such as camera movements, insects etc. Follow these steps to clean the results from artefacts:

- Open the Excel worksheet "Classified".
- Check the images stored in the "uncertain" folder. Decide for each image individually if the apparent brightness differences correspond to embolism events or not. In case of a false event, you can set the area of the corresponding image in the results file to zero and thus remove it from the results. In case of a true event, you can leave the entry for the area as it is. To facilitate this process, "uncertain" images are marked with an individual index, stored in the filename and behind the label in the spreadsheets.

Please note that the algorithm just searches for vertical aligned events. [Here](#about-labelling) you can find more information about the classifier.

#### Advice for better results:
- Define a ROI which only includes bare xylem and no bark or background
- Determine threshold and noise reduction parameters that eliminate as much noise as possible from the image.



### Dendro Analysis:
After the script has finished, results are saved in the following structure in the generated output directory:
- The "Processed" folder contains the processed images
- Results are written in a .csv and a .xlsx file and include the following parameters for each processed image: index, image name, time interval between captures, total time, and mean stem width

#### Advice for better results:
- Define a ROI which includes the full diameter of the stem but only a small part of the background
    - images get quickly darker as the distance from the image center increases, leading to a reduced contrast between the stem and the background
- The ROI needs to fully cover the stem diameter, but not the whole length of the image. A smaller ROI can produce better results because it reduces the probability of artefacts (e.g. insects in the ROI) 
- If you know about artefacts apparent in a certain area of your image time series try to avoid it with your ROI selection





## About labelling:

During the usual processing of image sequences, brightness changes between two subsequential images are determined on a pixel level.
To perform an automated event labelling for each image, pixels indicating changes are clustered spatially. Next, the individual groups are analysed. We defined certain criteria clusters must fulfil to count as cavitation events. For example, the ratio between the width and height of pixel groups is an important indicator. Embolism events typically exhibit thin and long differences in brightness due to stem xylem vessel structure. Additionally, the total size of and the pixel density within the clusters are utilized. The label "cavitation event" is set if all criteria are matched. However, if requirements are only partly fulfilled, an automated and unambiguous classification is not possible. Here, the label "uncertain" is provided to indicate individual decision-making is necessary. All other images are classified as "no event". 
Overall, the idea is to provide a trade-off between efficiency and accuracy. The classifier's accuracy is high, particularly in identifying large cavitation events that significantly impact the cumulative area of the whole stack. Small events, below the threshold of 300 pixels per cluster, may be categorized as "no event" in some cases. Individually checking "uncertain" events necessitates additional effort but significantly improves the final results.

## Citation:
Please consider citing us when you use CaviTools for your work:

> Giese, M., Ziegler, Y., & Ruehr, N. CaviTools: Automated processing of xylem cavitation and optical dendrometry image sequences (2024).
> https://doi.org/10.5281/zenodo.13328771

## Questions?
If you have questions, problems or suggestions regarding CaviTools, consider contacting us at mathis.giese2@kit.edu or yanick.ziegler@kit.edu.