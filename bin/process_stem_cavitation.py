from cavitools.frontend import CaviFrontend
from cavitools.file_manager import CaviFileManager
from cavitools.image_processor import CaviImageProcessor

def main():

    SAVE_INTERMEDIATES = False # saves raw image as a cropped and stabilized black and white version.
    SMOOTHING_WINDOW =  50 # number of images the image-stabilisation uses to detect shifts. (https://github.com/OpenSourceOV/cavicapture.git).
    LABELING = True #activates the cavitation classifier

    cavi_frontend = CaviFrontend()
    cavi_file_manager = CaviFileManager(cavi_frontend.set_input_dir())
    cavi_image_processor = CaviImageProcessor()

    cavi_file_manager.create_folder_structure(cavi_frontend.set_project_name(),SAVE_INTERMEDIATES,LABELING)
    image_data_list = cavi_file_manager.set_image_data_list()

    cropping_dict = cavi_frontend.set_cropping(image_data_list[3].path, cavi_image_processor)
    threshold, noise_reduction = cavi_frontend.set_processing_parameters(image_data_list,cropping_dict, False, cavi_image_processor,cavi_file_manager)

    image_data_list = cavi_image_processor.processAllImages(image_data_list, LABELING, threshold, noise_reduction, cropping_dict, False, SAVE_INTERMEDIATES, SMOOTHING_WINDOW, filemanager = cavi_file_manager)

    cavi_file_manager.save_cavi_image_data_list_as_xlsx(image_data_list)
    cavi_file_manager.save_cavi_image_data_list_as_csv(image_data_list)

if __name__ == "__main__":
    main()